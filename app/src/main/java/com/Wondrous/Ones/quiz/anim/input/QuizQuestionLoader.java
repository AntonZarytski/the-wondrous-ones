package com.Wondrous.Ones.quiz.anim.input;

import android.content.Context;
import android.content.res.Resources;

import com.Wondrous.Ones.quiz.anim.R;
import com.Wondrous.Ones.quiz.anim.entity.QuizQuestion;

import java.util.ArrayList;
import java.util.List;

public class QuizQuestionLoader {

    public static List<QuizQuestion> loadQuestions(Context context) {
        Resources res = context.getResources();
        String[] questionsArray = res.getStringArray(R.array.questions);
        String[] correctAnswersArray = res.getStringArray(R.array.answers);
        String[] variants1Array = res.getStringArray(R.array.variants_1);
        String[] variants2Array = res.getStringArray(R.array.variants_2);
        String[] variants3Array = res.getStringArray(R.array.variants_3);

        List<QuizQuestion> quizQuestions = new ArrayList<>();

        for (int i = 0; i < questionsArray.length; i++) {
            String option1 = correctAnswersArray[i];
            String option2 = variants1Array[i];
            String option3 = variants2Array[i];
            String option4 = variants3Array[i];
            quizQuestions.add(new QuizQuestion(questionsArray[i], option1, option2, option3, option4, option1));
        }

        return quizQuestions;
    }
}