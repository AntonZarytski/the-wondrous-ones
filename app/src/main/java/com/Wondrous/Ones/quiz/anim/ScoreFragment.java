package com.Wondrous.Ones.quiz.anim;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Wondrous.Ones.quiz.anim.databinding.ScoreBinding;

public class ScoreFragment extends Fragment {

    ScoreBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = ScoreBinding.inflate(inflater, container, false);

        if (getArguments() != null) {
            String score = getArguments().getString("score");
            binding.scoreTextView.setText(score);
        }

        binding.tryAgainButton.setOnClickListener(view -> {
            NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.action_scoreFragment_to_quizFragment);
        });

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}