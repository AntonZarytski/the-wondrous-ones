package com.Wondrous.Ones.quiz.anim.entity;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class QuizQuestion {
    private final String question;

    private final String option1;
    private final String option2;
    private final String option3;
    private final String option4;

    private final String correctAnswer;

    private String userAnswer;

    public QuizQuestion(@NonNull String question,
                        @NonNull String option1,
                        @NonNull String option2,
                        @NonNull String option3,
                        @NonNull String option4,
                        @NonNull String correctAnswer) {
        this.question = question;
        this.option1 = option1;
        this.option2 = option2;
        this.option3 = option3;
        this.option4 = option4;
        this.correctAnswer = correctAnswer;
    }

    @NonNull
    public List<String> getOptions() {
        List<String> options = new ArrayList<>();
        options.add(option1);
        options.add(option2);
        options.add(option3);
        options.add(option4);
        Collections.shuffle(options);
        return options;
    }

    @NonNull
    public String getQuestion() {
        return question;
    }

    public boolean isCorrectAnswer() {
        return correctAnswer.equals(userAnswer);
    }

    @NonNull
    public String getAnswer() {
        return correctAnswer;
    }

    @Nullable
    public String getUserAnswer() {
        return userAnswer;
    }

    public void setUserAnswer(@Nullable String answer) {
        userAnswer = answer;
    }

    public boolean isAnswered() {
        return userAnswer != null;
    }
}
