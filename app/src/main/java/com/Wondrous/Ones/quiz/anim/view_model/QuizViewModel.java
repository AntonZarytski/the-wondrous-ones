package com.Wondrous.Ones.quiz.anim.view_model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.Wondrous.Ones.quiz.anim.entity.QuizQuestion;
import com.Wondrous.Ones.quiz.anim.model.QuizModel;

import java.util.Objects;

public class QuizViewModel extends ViewModel {

    private final QuizModel model = QuizModel.getInstance();
    private final MutableLiveData<QuizQuestion> currentQuestion = new MutableLiveData<>();
    private final MutableLiveData<String> correctAnswer = new MutableLiveData<>();
    private final MutableLiveData<String> wrongAnswer = new MutableLiveData<>();
    private final MutableLiveData<String> score = new MutableLiveData<>();

    public void initQuiz() {
        model.reset();
        currentQuestion.setValue(model.getNextQuestion(null));
    }


    public void resume(int questionIndex, String answer) {
        QuizQuestion current = model.getQuestion(questionIndex);
        current.setUserAnswer(answer);
        currentQuestion.setValue(current);
        onAnswerReceived(Objects.requireNonNull(currentQuestion.getValue()).getUserAnswer());
    }

    public LiveData<QuizQuestion> getCurrentQuestion() {
        return currentQuestion;
    }

    public int getCurrentQuestionIndex() {
        return model.indexOf(currentQuestion.getValue());
    }

    public LiveData<String> getCorrectAnswer() {
        return correctAnswer;
    }

    public MutableLiveData<String> getScore() {
        return score;
    }

    public MutableLiveData<String> getWrongAnswer() {
        return wrongAnswer;
    }

    public void goToNextQuestion() {
        if (currentQuestion.getValue() != null && currentQuestion.getValue().isAnswered()) {
            QuizQuestion next = model.getNextQuestion(currentQuestion.getValue());
            if (next != null) {
                currentQuestion.setValue(next);
            } else {
                score.setValue(model.countCorrectAnswersCount() + "/" + model.getQuestionCount());
            }
        }
    }

    public void onAnswerReceived(String answer) {
        if (currentQuestion.getValue() != null && !currentQuestion.getValue().isAnswered()) {
            model.setAnswer(currentQuestion.getValue(), answer);
            correctAnswer.setValue(currentQuestion.getValue().getAnswer());
            if (!currentQuestion.getValue().isCorrectAnswer()) {
                wrongAnswer.setValue(answer);
            }
        }
    }
}