package com.Wondrous.Ones.quiz.anim.view_model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.Wondrous.Ones.quiz.anim.entity.QuizQuestion;
import com.Wondrous.Ones.quiz.anim.model.QuizModel;

import java.util.List;

public class LoadingViewModel extends ViewModel {

    private final QuizModel model = QuizModel.getInstance();
    public final MutableLiveData<Boolean> readyStatus = new MutableLiveData<>();

    public void setQuizQuestions(List<QuizQuestion> quizQuestions) {
        model.setQuizQuestions(quizQuestions);
        readyStatus.setValue(true);
    }
}
