package com.Wondrous.Ones.quiz.anim;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Wondrous.Ones.quiz.anim.databinding.MainMenuBinding;

public class MainMenuFragment extends Fragment {

    MainMenuBinding binding;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = MainMenuBinding.inflate(inflater, container, false);
        initListeners(binding);
        return binding.getRoot();
    }

    private void initListeners(@NonNull MainMenuBinding binding) {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        binding.startQuiz.setOnClickListener(v -> {
            navController.navigate(R.id.action_mainMenuFragment_to_quizFragment);
        });
        binding.policy.setOnClickListener(v -> {
            navController.navigate(R.id.action_mainMenuFragment_to_policyFragment);
        });
        binding.exit.setOnClickListener(v -> requireActivity().finish());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
