package com.Wondrous.Ones.quiz.anim.model;

import com.Wondrous.Ones.quiz.anim.entity.QuizQuestion;

import java.util.ArrayList;
import java.util.List;

public class QuizModel {

    private static QuizModel instance;

    public static QuizModel getInstance() {
        if (instance == null) {
            instance = new QuizModel();
        }
        return instance;
    }

    private QuizModel() {}

    List<QuizQuestion> quizQuestions = new ArrayList<>();

    public void setQuizQuestions(List<QuizQuestion> quizQuestions) {
        this.quizQuestions = quizQuestions;
    }

    public QuizQuestion getNextQuestion(QuizQuestion current){
        if (current == null) {
            return quizQuestions.get(0);
        }
        int newIndex = quizQuestions.indexOf(current) + 1;
        if (newIndex < quizQuestions.size()) {
            return quizQuestions.get(newIndex);
        } else {
            return null;
        }
    }

    public void setAnswer(QuizQuestion current, String answer) {
        QuizQuestion local = quizQuestions.get(quizQuestions.indexOf(current));
        local.setUserAnswer(answer);
    }

    public int countCorrectAnswersCount() {
        int correctCount = 0;
        for (QuizQuestion question : quizQuestions) {
            if (question.isCorrectAnswer()) {
                correctCount++;
            }
        }
        return correctCount;
    }

    public String getQuestionCount() {
        return String.valueOf(quizQuestions.size());
    }

    public int indexOf(QuizQuestion value) {
        return quizQuestions.indexOf(value);
    }

    public QuizQuestion getQuestion(int questionIndex) {
        return quizQuestions.get(questionIndex);
    }

    public void reset() {
        for (QuizQuestion question : quizQuestions) {
            question.setUserAnswer(null);
        }
    }
}
