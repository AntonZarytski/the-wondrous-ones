package com.Wondrous.Ones.quiz.anim;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Wondrous.Ones.quiz.anim.databinding.QuizBinding;
import com.Wondrous.Ones.quiz.anim.view_model.QuizViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class QuizFragment extends Fragment {

    private QuizViewModel viewModel;
    private QuizBinding binding;
    private String questionIndexKey;
    private String answerKey;
    private String scoreKey;

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        questionIndexKey = getString(R.string.question_index);
        answerKey = getString(R.string.answer);
        scoreKey = getString(R.string.score_key);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = QuizBinding.inflate(inflater, container, false);
        viewModel = new ViewModelProvider(this).get(QuizViewModel.class);
        viewModel.initQuiz();

        viewModel.getCurrentQuestion().observe(requireActivity(), question -> {
            List<String> options = question.getOptions();
            binding.questionTextView.setText(question.getQuestion());
            binding.answerVariant1.setText(options.get(0));
            binding.answerVariant2.setText(options.get(1));
            binding.answerVariant3.setText(options.get(2));
            binding.answerVariant4.setText(options.get(3));
            resetButtonsState();
        });

        viewModel.getCorrectAnswer().observe(requireActivity(), correctAnswer -> {
            List<Button> buttons = getButtons();
            for (Button button : buttons) {
                if (button.getText().toString().equals(correctAnswer)) {
                    setButtonColor(button, R.drawable.correct_answer);
                }
            }
        });

        viewModel.getWrongAnswer().observe(requireActivity(), wrongAnswer -> {
            List<Button> buttons = getButtons();
            for (Button button : buttons) {
                if (button.getText().toString().equals(wrongAnswer)) {
                    setButtonColor(button, R.drawable.wrong_answer);
                }
            }
        });

        viewModel.getScore().observe(requireActivity(), score -> {
            Bundle bundle = new Bundle();
            bundle.putString(scoreKey, score);
            NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
            navController.navigate(R.id.action_quizFragment_to_scoreFragment, bundle);
        });

        binding.nextQuestionButton.setOnClickListener(v -> viewModel.goToNextQuestion());
        if (savedInstanceState != null) {
            String answer = savedInstanceState.getString(answerKey, null);
            int questionIndex = savedInstanceState.getInt(questionIndexKey, 0);
            viewModel.resume(questionIndex, answer);
        }
        initListeners();
        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        int index = viewModel.getCurrentQuestionIndex();
        String answer = Objects.requireNonNull(viewModel.getCurrentQuestion().getValue()).getUserAnswer();
        outState.putInt(questionIndexKey, index);
        outState.putString(answerKey, answer);
    }

    @NonNull
    private List<Button> getButtons() {
        List<Button> buttons = new ArrayList<>();
        buttons.add(binding.answerVariant1);
        buttons.add(binding.answerVariant2);
        buttons.add(binding.answerVariant3);
        buttons.add(binding.answerVariant4);
        return buttons;
    }

    private void initListeners() {
        List<Button> buttons = getButtons();
        for (Button button : buttons) {
            setButtonListener(button);
        }
    }

    private void setButtonListener(@NonNull Button button) {
        button.setOnClickListener(v -> viewModel.onAnswerReceived(button.getText().toString()));
    }

    private void resetButtonsState() {
        List<Button> buttons = getButtons();
        for (Button button : buttons) {
            setButtonColor(button, R.drawable.button_selector);
        }
    }

    private void setButtonColor(@NonNull Button button, @DrawableRes int color) {
        button.setBackground(ContextCompat.getDrawable(requireContext(), color));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
