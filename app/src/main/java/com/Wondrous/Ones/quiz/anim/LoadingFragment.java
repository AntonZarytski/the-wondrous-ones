package com.Wondrous.Ones.quiz.anim;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.Wondrous.Ones.quiz.anim.databinding.LoadingBinding;
import com.Wondrous.Ones.quiz.anim.input.QuizQuestionLoader;
import com.Wondrous.Ones.quiz.anim.view_model.LoadingViewModel;

public class LoadingFragment extends Fragment {

    private LoadingBinding binding;
    private LoadingViewModel viewModel;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = LoadingBinding.inflate(inflater, container, false);
        viewModel = new ViewModelProvider(this).get(LoadingViewModel.class);
        viewModel.setQuizQuestions(QuizQuestionLoader.loadQuestions(requireContext()));
        viewModel.readyStatus.observe(requireActivity(), isReady -> {
            if (isReady) {
                toMainMenu();
            }
        });
        return binding.getRoot();
    }

    private void toMainMenu() {
        NavController navController = Navigation.findNavController(requireActivity(), R.id.nav_host_fragment);
        navController.navigate(R.id.action_loadingFragment_to_mainMenuFragment);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }
}
